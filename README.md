# OpenML dataset: Pokmon-Legendary-Data

https://www.openml.org/d/43379

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Context
In the world of Pokmon academia, one name towers above any other  Professor Samuel Oak. While his colleague Professor Elm specializes in Pokmon evolution, Oak has dedicated his career to understanding the relationship between Pokmon and their human trainers. A former trainer himself, the professor has first-hand experience of how obstinate Pokmon can be  particularly when they hold legendary status.
For his latest research project, Professor Oak has decided to investigate the defining characteristics of legendary Pokmon to improve our understanding of their temperament. Hearing of our expertise in classification problems, he has enlisted us as the lead researchers.
Our journey begins at the professor's research lab in Pallet Town, Kanto. The first step is to open up the Pokdex, an encyclopaedic guide to 801 Pokmon from all seven generations.

Content
After browsing the Pokdex, we can see several variables that could feasibly explain what makes a Pokmon legendary. We have a series of numerical fighter stats  attack, defense, speed and so on  as well as a categorization of Pokemon type (bug, dark, dragon, etc.). is_legendary is the binary classification variable we will eventually be predicting, tagged 1 if a Pokmon is legendary and 0 if it is not.
Before we explore these variables in any depth, let's find out how many Pokmon are legendary out of the 801 total, using the handy count() function from the dplyr package.

Acknowledgements
DataCamp

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43379) of an [OpenML dataset](https://www.openml.org/d/43379). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43379/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43379/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43379/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

